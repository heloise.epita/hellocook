import java.awt.*;
import java.util.ArrayList;

public class Book {
    private ArrayList recipes = new ArrayList();
    public Book() {}

    public ArrayList getRecipes() {
        return recipes;
    }
    public void setRecipes(ArrayList recipes) {
        this.recipes = recipes;
    }

    @Override
    public String toString() {
        return "Book{" +
                "recipes=" + recipes +
                '}';
    }
}
