import java.util.List;

public class Recipes {
    private String type;
    private float time;
    private int difficulty;
    private List<String> ingredients;
    private List<String> instructions;

    public Recipes(){}
    public Recipes(String type, float time,int difficulty) {
        this.type = type;
        this.time = time;
        this.difficulty = difficulty;
    }


    public String getType() {
        return type;
    }
    public float getTime() {
        return time;
    }
    public int getDifficulty() {
        return difficulty;
    }
    public List<String> getIngredients() {
        return ingredients;
    }
    public List<String> getInstructions() {
        return instructions;
    }



    public void setName(String type) {
        this.type = type;
    }
    public void setTime(int time) {
        this.time = time;
    }
    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }
    public void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }
    public void setInstructions(List<String> instructions) {
        this.instructions = instructions;
    }





}
