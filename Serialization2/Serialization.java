import com.sun.xml.txw2.output.XmlSerializer;
import java.beans.XMLEncoder;
import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

public class Serialization {

    private static final String SERIALIZED_FILE_NAME="book.xml";

    static void Main(String[] args)
    {

        Recipes Flan = new Recipes("Dessert", 0.5f,3);
        Recipes Gratin = new Recipes("Plat", 1.5f,5);
        Recipes Café = new Recipes("Boisson", 0.15f,1);

        ArrayList recipesList = new ArrayList();
        recipesList.add(Flan);
        recipesList.add(Gratin);
        recipesList.add(Café);

        Book Etudiants = new Book();
        Etudiants.setRecipes(recipesList);


        XMLEncoder encoder = null;
        try{
            encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(SERIALIZED_FILE_NAME)));
        }
        catch(FileNotFoundException fileNotFound) {
            System.out.println("ERROR: While Creating or Opening the File dvd.xml");
        }
        encoder.writeObject(Etudiants);
        encoder.close();
    }
}
