﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace SerialisationApp.Models
{
    public class Bibliothèque
    {
        public List<Livre> Livres;
        public Bibliothèque BibliothequeAdjacente;
        public String Theme;
        public int Taille
        {
            get
            {
                return Livres.Count;
            }
        }

        public Bibliothèque()
        {
            this.Livres = new List<Livre>();
        }

        public static Bibliothèque GetLib ()
        {
            var lib = new Bibliothèque();
            lib.Theme = "Inclassable";

            for (int i = 0; i < 5; i++)
                lib.Livres.Add(GetLivre());
            return lib;
        }

        public void WriteLine()
        {
            Console.WriteLine("Bibliothèque - thème : {0}, taille : {1}", Theme, Taille);
            Console.WriteLine("Contenu : ");
            Livres.ForEach(l => l.WriteLine());
            Console.WriteLine("Fin de contenu");
        }
    }
}