﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using SerialisationApp.Models;

namespace SerializationApp
{
    class Program
    {


        public static class EnumerableExtension
        {
            public static T GetRandom<T>(this IList<T> list)
            {
                //En cas de génération de plusieurs nombres aléatoires, évite que le random se base sur le même seed.
                Thread.Sleep(11);

                Random r = new Random();
                return list[r.Next(list.Count)];
            }
        }



        //GENERE PREMIER FICHIER XML
        static void Main(string[] args)
        {
            //1. Genere librairie aléatoire
            Bibliothèque lib = Bibliothèque.GetLib(); 
            Console.WriteLine("Test de sérialisation");

            //2. Creation d'une nouvelle instance XmlSerializer. C'est l'objet qui va se charger de serializer/ deserializer
            XmlSerializer serializer = new XmlSerializer(typeof(Bibliothèque));

            //3. StreamWriter : ouvrir un fichier sur le disque.
            using (StreamWriter writer = new StreamWriter("Bibliotheque.xml"))
            {
                serializer.Serialize(writer, lib);
                lib.WriteLine();
            }
            Console.WriteLine("\nAppuyez sur une touche pour terminer le programme");
            Console.ReadKey();

        }
    }
}
